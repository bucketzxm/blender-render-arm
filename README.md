# Multi-Node Parallel Blender Rendering

* `bash-script`: Convenient Bash Scripts
    * `gen_copy_ssh_key.bash`: Generate and copy SSH-keys to multiple nodes
* `python-fabric`: Python Fabric System Application Deployment Scripts
    * `installBlender_fab.py`: Install Blender onto multiple nodes
        * Eg: `fab -f installBlender_fab.py enable_swap_sdcard copy_blender_source build_blender_source`
    * `render_fab.py`: Render a blender file with multiple nodes
        * Eg: `fab -f render_fab.py clean_render_output render:filename=blenderfile.blend,start=1,end=250 fetch_render_output extract`

