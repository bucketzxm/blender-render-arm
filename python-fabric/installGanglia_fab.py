#!/bin/bash/env python

from fabric.api import *
import os

env.hosts = [
    '192.168.1.101',
    '192.168.1.102',
    '192.168.1.103',
]
env.user = 'ubuntu'
env.password = 'ubuntu'
WORK_DIR = '/home/ubuntu/ganglia-source/'


@parallel
def install_ganglia():
    """install ganglia to multiple nodes

    usage: fab -f install_ganglia
    :keyword:
    :return:
    """
    sudo('apt-get -y install ganglia-monitor ganglia-monitor-python gmetad')
    run('mkdir -p {}'.format(WORK_DIR))
    put("./resource/nvidia-ml-py*.tar.gz", WORK_DIR)
    with cd(WORK_DIR):
        run('tar xzf nvidia-ml-py*.tar.gz')
        with cd('nvidia-ml-py-7.352.0'):
            sudo('python setup.py install')


@parallel
def add_gpu_gmond():
    """ Add gmond ganglia module to multiple nodes
    :return: None
    """
    GANGLIA_LIB_DIR = '/usr/lib/ganglia/'
    run('mkdir -p {}'.format(WORK_DIR))
    with cd(WORK_DIR):
        run('git clone https://github.com/ganglia/gmond_python_modules.git')
        with cd('./gmond_python_modules/gpu/nvidia'):
            sudo('cp python_modules/nvidia.py {}'.format(os.path.join(GANGLIA_LIB_DIR, 'python_modules')))
            sudo('cp conf.d/nvidia.pyconf /etc/ganglia/conf.d')
    sudo('service ganglia-monitor restart')


@parallel
def clean_gpu_gmond():
    """
    clean gmond ganglia module
    :return:
    """
    run('cd {}'.format(WORK_DIR))
    with cd(WORK_DIR):
        run('rm -rf gmond_python_modules')


@parallel
def clean_apt_lock():
    """ clean apt-get lock

    :return: NOne
    """
    sudo('rm -f /var/lib/dpkg/lock')
    sudo('rm -f /var/cache/apt/archives/lock')
    sudo('dpkg --configure -a')
