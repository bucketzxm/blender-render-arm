#!/bin/bash/env python

from __future__ import print_function
from copy import deepcopy
import sys

from fabric.api import *

env.hosts = [
    #192.168.1.101',
    '192.168.1.102',
    #'192.168.1.103',
    #'192.168.1.104',
    #'192.168.1.105',
]
env.user = 'ubuntu'
env.password = 'ubuntu'

class ShellEnvManager:
    def __init__(self):
        self.env = []

    def __str__(self):
        ret = ''
        for e in self.env:
            ret += str(e) + '\n'
        return ret

    def __iter__(self):
        for i, e in enumerate(self.env):
            assert(len(e) == 2), 'env should be lists of length == 2: '.format(e)
            yield e

    def find(self, key):
        for i, e in enumerate(self.env):
            assert(len(e) == 2), 'env should be lists of length == 2: '.format(e)
            if e[0] == key:
                return i, e
        return None

    def get(self, key):
        f = self.find(key)
        return None if f is None else f[1][1]

    def set(self, key, value):
        f = self.find(key)
        if not f is None:
            self.env[f[0]][1] = value
            return
        self.env.append([key, value])

build_blender_env = ShellEnvManager()
build_blender_env.set('OPENEXR_VERSION', "2.2.0")
build_blender_env.set('OCIO_VERSION', "1.0.9")
build_blender_env.set('OIIO_VERSION', "1.4.16")
build_blender_env.set('OSL_VERSION', "1.5.11")
build_blender_env.set('OCIO_VERSION', "1.0.9")
build_blender_env.set('ILMBASE_VERSION', "2.2.0")
build_blender_env.set('BOOST_VERSION', "1.55")
build_blender_env.set('OIIO_VERSION', "1.4.16")
build_blender_env.set('LLVM_VERSION', "3.4")

def get_WORK_DIR():
    whoami = run('whoami')
    return '/home/{}/blender-source/'.format(whoami)

@parallel
def enable_swap():
    swap_block_num = 8*1024*1024
    swap_filename = "swap"
    sudo('swapoff -a')
    with cd('/'):
        ret = run('[ -f "{}" ] && echo "exist" || echo "nonexist"'.format(swap_filename))
        if ret == 'nonexist':
            sudo('touch "{}"'.format(swap_filename))
            sudo('dd if=/dev/zero of="{}" bs=1024 count={}'.format(swap_filename, swap_block_num))
            sudo('mkswap "{}" {}'.format(swap_filename, swap_block_num))
        sudo('swapon "{}"'.format(swap_filename))

@parallel
def enable_swap_sdcard():
    sudo('swapoff -a')
    sdcard_device = "/dev/mmcblk1"
    whoami = run('whoami')
    sudo('swapon "{}"'.format(sdcard_device))

@parallel
def stop_ganglia_monitor():
    sudo('service ganglia-monitor stop', warn_only=True)

@parallel
def apt_get_update():
    sudo('apt-get update')

@parallel
def clean_blender_source():
    WORK_DIR = get_WORK_DIR()
    sudo('rm -rf ' + WORK_DIR)
    sudo('rm -rf /opt/lib')

@parallel
def copy_blender_source():
    WORK_DIR = get_WORK_DIR()
    run('mkdir -p ' + WORK_DIR)
    with cd(WORK_DIR):
        put('./resource/blender-git.tar.gz', './blender-git.tar.gz')
        run('tar -xzf ./blender-git.tar.gz')
        run('rm -f ./blender-git.tar.gz')

@parallel
def run_with_env(env, command):
    command_with_env = ''
    for e in env:
        command_with_env += 'export {}="{}";'.format(e[0], e[1])
    command_with_env += command
    return run(command_with_env)

@parallel
def sudo_with_env(env, command):
    command_with_env = ''
    for e in env:
        command_with_env += 'export {}="{}";'.format(e[0], e[1])
    command_with_env += command
    return sudo(command_with_env)

@parallel
def build_ocio():
    '''
    $HOME/src softlink required

    @see build_blender_source for more info
    '''
    ocio_env = deepcopy(build_blender_env)
    ocio_env.set('INST', '/opt/lib/')
    ocio_env.set('_inst', '$INST/ocio-$OCIO_VERSION/')
    with cd('$HOME/src/blender-deps/OpenColorIO-1.0.9/'):
        run('mkdir -p build/')
        with cd('build/'):
            run_with_env(ocio_env,
                         'cmake \
                              -D CMAKE_BUILD_TYPE=Release \
                              -D CMAKE_EXE_LINKER_FLAGS="-lgcc_s -lgcc" \
                              -D CMAKE_PREFIX_PATH="$_inst"\
                              -D CMAKE_INSTALL_PREFIX="$_inst"\
                              -D OCIO_BUILD_APPS=OFF\
                              -D OCIO_BUILD_PYGLUE=OFF \
                              -D CMAKE_CXX_FLAGS="-fPIC -mfpu=\'neon\'" \
                              -D OCIO_USE_SSE=OFF \
                          ../')
            sudo('make -j4')
            sudo('make install')
            sudo_with_env(ocio_env, 'touch "/opt/lib/.ocio-$OCIO_VERSION-magiccheck-1"')

@parallel
def build_osl():
    '''
    $HOME/src softlink required

    @see build_blender_source for more info
    '''
    osl_env = deepcopy(build_blender_env)
    osl_env.set('LC_ALL', 'C')
    osl_env.set('INST', '/opt/lib/')
    osl_env.set('_inst', "$INST/osl-$OSL_VERSION")
    with cd('$HOME/src/blender-deps/OpenShadingLanguage-1.5.11/'):
        run('mkdir -p build/')
        with cd('build/'):
            sudo_with_env(osl_env, 'ln -sf /opt/lib/oiio-$OIIO_VERSION /opt/lib/oiio')
            run_with_env(osl_env,
                         'cmake \
                              -D CMAKE_INSTALL_PREFIX="$_inst" \
                              -D BUILD_TESTING=OFF \
                              -D STOP_ON_WARNING=OFF \
                              -D BUILDSTATIC=OFF \
                              -D ILMBASE_CUSTOM_LIBRARIES=\'Half;Iex;Imath;IlmThread\' \
                              -D BOOST_ROOT="$INST/boost" -D Boost_NO_SYSTEM_PATHS=ON \
                              -D OPENIMAGEIOHOME="$INST/oiio" \
                              -D LLVM_VERSION=$LLVM_VERSION \
                              -D OPENIMAGEIOHOME=$INST/oiio \
                              -D CMAKE_EXPORT_COMPILE_COMMANDS=ON \
                              -D CMAKE_VERBOSE_MAKEFILE=ON \
                          ../')
            run('make -j4')
            sudo('make install')
            sudo_with_env(osl_env, 'ln -sf /opt/lib/osl-$OSL_VERSION /opt/lib/osl')
            sudo_with_env(osl_env, 'touch "/opt/lib/.osl-$OSL_VERSION-magiccheck-17"')

@parallel
def build_ilmbase():
    '''
    $HOME/src softlink required

    @see build_blender_source for more info
    '''
    ilmbase_env = deepcopy(build_blender_env)
    ilmbase_env.set('CPLUS_INCLUDE_PATH', '-I/usr/include/arm-linux-gnueabihf/c++/4.8/')
    ilmbase_env.set('LIBRARY_PATH', '/usr/lib/arm-linux-gnueabihf/')
    ilmbase_env.set('INST', '/opt/lib')
    ilmbase_env.set('_inst', '$INST/ilmbase-$ILMBASE_VERSION')
    ilmbase_env.set('_inst_shortcut', '$INST/ilmbase')
    ilmbase_env.set('cflags', '-fPIC -mfloat-abi=softfp -mfpu=neon -march=armv7')

    with cd('$HOME/src/blender-deps/ILMBase-{}'.format(ilmbase_env.get('ILMBASE_VERSION'))):
        run('mkdir -p build')
        with cd('build/'):
            run_with_env(ilmbase_env, 'cmake \
                                   -D CMAKE_BUILD_TYPE=Release \
                                   -D CMAKE_INSTALL_PREFIX=$_inst \
                                   -D BUILD_SHARED_LIBS=ON \
                                   -D NAMESPACE_VERSIONING=OFF \
                                   -D CMAKE_CXX_FLAGS="$cflags" \
                                   -D CMAKE_EXE_LINKER_FLAGS="-lgcc_s -lgcc" \
                               ../')
            run('make -j4')
            sudo('make install')
    sudo_with_env(ilmbase_env, 'rm -f $_inst_shortcut')
    sudo_with_env(ilmbase_env, 'echo $_inst_shortcut; ln -sf $_inst $_inst_shortcut')
    sudo('echo "/opt/lib/ilmbase/lib/" > /etc/ld.so.conf.d/ilmbase.conf')
    sudo('ldconfig')
    sudo_with_env(ilmbase_env, 'touch "$INST/.ilmbase-$ILMBASE_VERSION-magiccheck-10"')

@parallel
def build_openexr():
    '''
    $HOME/src softlink required

    @see build_blender_source for more info
    '''
    openexr_env = deepcopy(build_blender_env)
    openexr_env.set('_ilmbase_inst', '/opt/lib/ilmbase/')
    openexr_env.set('INST', '/opt/lib/')
    openexr_env.set('_openexr_inst', '$INST/openexr-$OPENEXR_VERSION')
    openexr_env.set('_inst', '$_openexr_inst')
    openexr_env.set('_inst_shortcut', '$INST/openexr')
    openexr_env.set('cflags', '-fPIC -march=armv7')

    with cd('$HOME/src/blender-deps/OpenEXR-{}'.format(openexr_env.get('OPENEXR_VERSION'))):
        run('mkdir -p build')
        with cd('build/'):
            run_with_env(openexr_env,
                         'cmake \
                              -D CMAKE_BUILD_TYPE=Release \
                              -D CMAKE_PREFIX_PATH="$_inst" \
                              -D CMAKE_INSTALL_PREFIX="$_inst" \
                              -D ILMBASE_PACKAGE_PREFIX="$_ilmbase_inst" \
                              -D BUILD_SHARED_LIBS="ON" \
                              -D NAMESPACE_VERSIONING="OFF" \
                              -D CMAKE_CXX_FLAGS="$cflags" \
                              -D CMAKE_EXE_LINKER_FLAGS="-lgcc_s -lgcc" \
                          ../')
            # This is a bug:
            #   if OpenEXR is configured without neon, it will not link to ILMBase
            #   if OpenEXR is configured with neon, then it'll not be able to find zlib
            # So, solution is to make it first configure without neon, to find zlib
            # and then, reconfigure it with neon in order to link to ILMBase
            openexr_env.set('cflags', '-fPIC -mfloat-abi=softfp -mfpu=neon -march=armv7')
            run_with_env(openexr_env,
                         'cmake \
                              -D CMAKE_BUILD_TYPE=Release \
                              -D CMAKE_PREFIX_PATH="$_inst" \
                              -D CMAKE_INSTALL_PREFIX="$_inst" \
                              -D ILMBASE_PACKAGE_PREFIX="$_ilmbase_inst" \
                              -D BUILD_SHARED_LIBS="ON" \
                              -D NAMESPACE_VERSIONING="OFF" \
                              -D CMAKE_CXX_FLAGS="$cflags" \
                              -D CMAKE_EXE_LINKER_FLAGS="-lgcc_s -lgcc" \
                          ../')

            openexr_env.set('CPLUS_INCLUDE_PATH', '/usr/include/arm-linux-gnueabihf/:$_ilmbase_inst/include/OpenEXR/')
            run_with_env(openexr_env, 'make -j4')
            sudo('make install')
    sudo_with_env(openexr_env, 'rm -f $_inst_shortcut')
    sudo_with_env(openexr_env, 'ln -sf $_inst $_inst_shortcut')
    sudo_with_env(openexr_env, 'cp -Lrn $_ilmbase_inst/* $_inst_shortcut')
    sudo_with_env(openexr_env, 'touch "$INST/.openexr-$OPENEXR_VERSION-magiccheck-13"')

@parallel
def build_blender_source():
    def check_mem(expectSize):
        '''
        check size in KB
        '''
        memSize = run('cat /proc/meminfo | grep MemTotal | awk \'{ print $2 }\'')
        swapSize = run('cat /proc/meminfo | grep SwapTotal | awk \'{ print $2 }\'')
        total = int(memSize) + int(swapSize)
        return total >= expectSize

    if check_mem(6291456) == False:
        print('Memory+Swap is less than 6G, Please enable swap before build', file=sys.stderr)
        return

    sudo('apt-get -y install build-essential cmake libc-dev g++-multilib llvm-3.4')

    WORK_DIR = get_WORK_DIR()
    run('mkdir -p ' + WORK_DIR)

    # 'blender/build_files/build_environment/install_deps.sh' installs deps into '$HOME/src/' directory,
    # so after extraction, we must soft link 'src' directory to '$HOME/src'
    with cd(WORK_DIR):
        sudo('rm -rf $HOME/src')
        run('ln -s $HOME/blender-source/src $HOME/src')
        sudo('chown -R {} .'.format(env.user))

    # Create install target dir
    sudo('mkdir -p /opt/lib/')
    sudo('chown -R {} /opt/lib/'.format(env.user))
    # build ILMBase/OpenEXR manually
    build_ilmbase()
    build_openexr()

    # Install and build other dependencies (Skip those not working)
    with cd(WORK_DIR):
        with cd('blender/build_files/build_environment/'):
            sudo('echo "Y" | ./install_deps.sh --threads=4 --build-boost --skip-ocio --skip-osl --skip-openexr')

    build_ocio()
    build_osl()

    with cd(WORK_DIR):
        # Build Blender Cycles with CUDA Support
        with cd('blender/build_files/build_environment/'):
            # Build the whole deps
            sudo('echo "Y" | ./install_deps.sh --threads=4 --build-boost --skip-openexr')
        with cd('blender'):
            make_env = ShellEnvManager()
            make_env.set('C_INCLUDE_PATH', '/usr/include/arm-linux-gnueabihf/:/opt/lib/openexr/include/OpenEXR/')
            make_env.set('CPLUS_INCLUDE_PATH', '/usr/include/arm-linux-gnueabihf/:/opt/lib/openexr/include/OpenEXR/')
            make_env.set('cflags', '-fPIC -mfloat-abi=softfp -mfpu=neon -march=armv7')
            sudo_with_env(make_env, 'make -j4 BUILD_CMAKE_ARGS=" \
                    -D CMAKE_CXX_FLAGS=\'$cflags\' \
                    -D WITH_CODEC_SNDFILE=ON \
                    -D PYTHON_VERSION=3.5 \
                    -D PYTHON_ROOT_DIR=/opt/lib/python-3.5 \
                    -D WITH_OPENEXR=/opt/lib/openexr \
                    -D OPENIMAGEIO_ROOT_DIR=/opt/lib/oiio \
                    -D WITH_CYCLES_OSL=OFF \
                    -D WITH_LLVM=OFF \
                    -D WITH_OPENSUBDIV=ON \
                    -D OPENSUBDIV_ROOT_DIR=/opt/lib/osd \
                    -D WITH_CODEC_FFMPEG=ON \
                    -D FFMPEG_LIBRARIES=\'avformat;avcodec;avutil;avdevice;swscale;rt;theora;theoraenc;theoradec;vorbis;vorbisfile;vorbisenc;ogg;x264;openjpeg\' \
                    -D FFMPEG=/opt/lib/ffmpeg \
                    -D WITH_CYCLES_CUDA_BINARIES=ON \
                    -D CYCLES_CUDA_BINARIES_ARCH=sm_32"')
            sudo('ldconfig')

    # remove the soft link to cleanup
    run('rm -f $HOME/src')

@parallel
def clean_apt_lock():
    sudo('rm -f /var/lib/dpkg/lock')
    sudo('rm -f /var/cache/apt/archives/lock')
