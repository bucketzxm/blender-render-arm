# Arch Blender Build

## Require packages

```
sudo apt-get install binutils libc-dev llvm-3.4
```

## Requires ILMBase-2.2.0

```
cd $HOME/src/blender-deps/ILMBase-2.2.0/

mkdir -p build/
cd build/

ILMBASE_VERSION="2.2.0"
CPLUS_INCLUDE_PATH="-I/usr/include/arm-linux-gnueabihf/c++/4.8/"
LD_LIBRARY_PATH="/usr/lib/arm-linux-gnueabihf/"
LIBRARY_PATH="/usr/lib/arm-linux-gnueabihf/"
INST="/opt/lib/"
_inst=$INST/ilmbase-$ILMBASE_VERSION
_inst_shortcut=$INST/ilmbase
cflags="-fPIC -mfloat-abi=softfp -mfpu=neon -march=armv7"
cmake \
  -D CMAKE_BUILD_TYPE=Release \
  -D CMAKE_PREFIX_PATH=$_inst \
  -D CMAKE_INSTALL_PREFIX=$_inst \
  -D BUILD_SHARED_LIBS=ON \
  -D NAMESPACE_VERSIONING=OFF \
  -D CMAKE_CXX_FLAGS="$cflags" \
  -D CMAKE_EXE_LINKER_FLAGS="-lgcc_s -lgcc" \
../
make -j4
make install

ln -sf $_inst $_inst_shortcut
touch "$INST/.ilmbase-$ILMBASE_VERSION-magiccheck-10"
```

## Requires OpenEXR-2.2.0

```
cd $HOME/src/blender-deps/OpenEXR-2.2.0/

mkdir -p build/
cd build/
OPENEXR_VERSION="2.2.0"
_ilmbase_inst=/opt/lib/ilmbase/
INST="/opt/lib/"
_openexr_inst=$INST/openexr-$OPENEXR_VERSION
_inst=$_openexr_inst
_inst_shortcut=$INST/openexr
cflags="-fPIC -march=armv7"

cmake \
    -D CMAKE_BUILD_TYPE=Release \
    -D CMAKE_PREFIX_PATH="$_inst" \
    -D CMAKE_INSTALL_PREFIX="$_inst" \
    -D ILMBASE_PACKAGE_PREFIX="$_ilmbase_inst" \
    -D BUILD_SHARED_LIBS="ON" \
    -D NAMESPACE_VERSIONING="OFF" \
    -D CMAKE_CXX_FLAGS="$cflags" \
    -D CMAKE_EXE_LINKER_FLAGS="-lgcc_s -lgcc" \
../
make -j4
make install

ln -sf $_inst $_inst_shortcut
cp -Lrn $_ilmbase_inst/* $_inst_shortcut
touch "$INST/.openexr-$OPENEXR_VERSION-magiccheck-13"
```

## Requires OpenSubdiv-3.0.2

1. Extract it
2. edit `opensubdiv/CMakeLists.txt`
3. change string `compute_11` to `compute_20`

## Requires OpenColorIO-1.0.9

```
cd $HOME/src/blender-deps/OpenColorIO-1.0.9/

mkdir -p build/
cd build/
OCIO_VERSION="1.0.9"
INST=/opt/lib
_inst=$INST/ocio-$OCIO_VERSION/
cmake \
  -D CMAKE_BUILD_TYPE=Release \
  -D CMAKE_EXE_LINKER_FLAGS="-lgcc_s -lgcc" \
  -D CMAKE_PREFIX_PATH="$_inst" \
  -D CMAKE_INSTALL_PREFIX="$_inst" \
  -D OCIO_BUILD_APPS=OFF \
  -D OCIO_BUILD_PYGLUE=OFF \
  -D CMAKE_CXX_FLAGS="-fPIC -mfpu='neon'" \
  -D OCIO_USE_SSE=OFF \
../
make -j4
make install
touch "/opt/lib/.ocio-$OCIO_VERSION-magiccheck-1"
```

## Requires OpenShadingLanguage

```
cd $HOME/src/blender-deps/OpenShadingLanguage-1.5.11/

cd src/testshade/
patch < $TODO/osl.1.5.11.testshade.cpp.patch
cd ../../

cd build/
export LC_ALL=C
OSL_VERSION="1.5.11"
OCIO_VERSION="1.0.9"
ILMBASE_VERSION="2.2.0"
BOOST_VERSION="1.55"
OIIO_VERSION="1.4.16"
LLVM_VERSION="3.4"
INST=/opt/lib
_inst="$INST/osl-$OSL_VERSION"
ln -sf /opt/lib/oiio-$OIIO_VERSION /opt/lib/oiio
cmake \
  -D CMAKE_INSTALL_PREFIX="$_inst" \
  -D BUILD_TESTING=OFF \
  -D STOP_ON_WARNING=OFF \
  -D BUILDSTATIC=OFF \
  -D ILMBASE_VERSION="$ILMBASE_VERSION" \
  -D ILMBASE_HOME="$INST/openexr" \
  -D ILMBASE_CUSTOM=ON \
  -D ILMBASE_CUSTOM_LIBRARIES='Half;Iex;Imath;IlmThread' \
  -D BOOST_ROOT="$INST/boost" -D Boost_NO_SYSTEM_PATHS=ON \
  -D OPENIMAGEIOHOME="$INST/oiio" \
  -D LLVM_VERSION=$LLVM_VERSION \
  -D OPENIMAGEIOHOME=$INST/oiio \
  -D CMAKE_EXPORT_COMPILE_COMMANDS=ON \
  -D CMAKE_VERBOSE_MAKEFILE=ON \
../
make -j4
make install
ln -sf /opt/lib/osl-$OSL_VERSION /opt/lib/osl
touch "/opt/lib/.osl-$OSL_VERSION-magiccheck-17"
```

## Install Deps

```
bash ./install_deps.sh --build-boost --build-oiio
```

## Make blender

```
make -j4 BUILD_CMAKE_ARGS=" \
  -D WITH_CODEC_SNDFILE=ON \
  -D PYTHON_VERSION=3.5 \
  -D PYTHON_ROOT_DIR=/opt/lib/python-3.5 \
  -D OPENEXR_ROOT_DIR=/opt/lib/openexr \
  -D OPENIMAGEIO_ROOT_DIR=/opt/lib/oiio \
  -D WITH_CYCLES_OSL=ON \
  -D CYCLS_OSL=/opt/lib/osl \
  -D WITH_LLVM=ON \
  -D LLVM_CONFIG=/usr/bin/llvm-config-3.4 \
  -D LLVM_LIBRARY=/usr/lib/llvm-3.4/ \
  -D WITH_OPENSUBDIV=ON \
  -D OPENSUBDIV_ROOT_DIR=/opt/lib/osd \
  -D WITH_CODEC_FFMPEG=ON \
  -D FFMPEG_LIBRARIES='avformat;avcodec;avutil;avdevice;swscale;rt;theora;theoraenc;theoradec;vorbis;vorbisfile;vorbisenc;ogg;x264;openjpeg' \
  -D FFMPEG=/opt/lib/ffmpeg"
  -D WITH_CYCLES_CUDA_BINARIES=ON
  -D CYCLES_CUDA_BINARIES_ARCH=sm_32
sudo ldconfig
```

