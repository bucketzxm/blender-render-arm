from fabric.api import *
from render_fab import read_server_config, under_configured_user


HOST_NUM = 0
HOST_INFO = {}

read_server_config('server.json')

@parallel
@under_configured_user
def shutdown():
    sudo('shutdown -h now')


if __name__ == "__main__":
    print('fab -f shutdown.py shutdown')



