#!/bin/bash/env python
from __future__ import print_function
import os
import fnmatch
from fabric.api import *
import json
import argparse

WORK_DIR = '/home/ubuntu/render_work'
BLENDER_PATH = '$HOME/blender-source/build_linux/bin/'
HOST_NUM = 0
HOST_INFO = {}


def read_server_config(config_filename="server.json"):
    """
    read hostname, username, password from config file
    :param config_filename: file where server info record in
    :return:
    """
    with open(config_filename, "r") as f:
        res = json.load(f)
        for h in res:
            host = res.get(h)
            env.hosts.append(host['hostname'])
            env.passwords[host['hostname']] = host['password']
            env.roledefs[host['hostname']] = {
                'hosts': [host['hostname']],
                'username': host['username'],
                'password': host['password']
            }
        global HOST_NUM
        HOST_NUM = len(res)

read_server_config('server.json')


def under_configured_user(fn):
    def wrapper(*args, **kwargs):
        user = env.roledefs[env.host]['username']
        password = env.roledefs[env.host]['password']
        with settings(user=user, password=password):
            fn(*args, **kwargs)
    return wrapper


@parallel
@under_configured_user
def print_server_ip():
    run('ifconfig | grep addr: | line 1')


@parallel
@under_configured_user
def clean_render_output():
    run('rm -rf /home/ubuntu/render_work/output')


@parallel
@under_configured_user
def render(filename, start, end):
    try:
        start = int(start)
        end = int(end)
    except ValueError:
        print("ValueError: start/end is not integer (start={}, end={})".format(start, end))

    def gen_render_cmd(filename, start, end, engine='BLENDER_RENDER', output_name='$HOME/render_work/output/render'):
        output_name = '{}_{}_'.format(output_name, start)
        blender_cmd = BLENDER_PATH + 'blender -b {filename} -o {output} -E {ENGINE} -s {start} -e {end} -a'
        blender_cmd = blender_cmd.format(filename=filename, start=start, end=end, ENGINE=engine, output=output_name)
        return blender_cmd

    run('mkdir -p %s' % WORK_DIR)
    host_id = env.hosts.index(env.host)
    frames_per_node = (end - start + 1) / HOST_NUM
    frame_start = host_id * frames_per_node + 1
    frame_end = (host_id + 1) * frames_per_node
    if host_id == HOST_NUM-1:
        frame_end = end
    render_cmd = gen_render_cmd(filename, frame_start, frame_end)
    with cd(WORK_DIR):
        run('mkdir -p output')
        put(filename, os.path.basename(filename))
        run(render_cmd)


@parallel
@under_configured_user
def fetch_render_output():
    def fetch_each():
        with cd(os.path.join(WORK_DIR, 'output')):
            output_tar = 'output-{}.tar.gz'.format(env.host)
            run('tar czf {} *'.format(output_tar))
            get(output_tar, os.path.join('output', output_tar))

    run('mkdir -p output')
    fetch_each()


def extract():
    tar_files = [f for f in os.listdir('output') if fnmatch.fnmatch(f, '*.tar.gz')]
    with lcd('output'):
        map(lambda f: local('tar xzf ' + f), tar_files)
    local('echo "Extraction Done!"')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Tasks running on clusters")
    parser.add_argument('--config', help='server info config filename, using json format to '
                                         'record hostname, username and password')
    parser.add_argument('--task', help='run task on clusters')
    args = parser.parse_args()
    try:

        task, params = args.task.split(':')
        param_list = [param.split('=') for param in params.split(',')]
        param_keys = {key: value for (key, value) in param_list}

        read_server_config(args.config)
        eval(task)(param_keys)
    except TypeError as e:
        print(e.message)
